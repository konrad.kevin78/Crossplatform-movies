import { FavoriteMovieProvider } from "../../providers/favorite-movie/favorite-movie";
import { MovieDetailPage } from "../movie-detail/movie-detail";
import { IMovie } from "../../interface/IMovie";
import { MovieListPage } from "../movie-list/movie-list";
import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-my-movies",
  templateUrl: "my-movies.html"
})
export class MyMoviesPage {
  private favoriteMovies: IMovie[];

  constructor(private navCtrl: NavController, private favoriteMovieProvider: FavoriteMovieProvider) {}

  ionViewDidLoad() {
    this.initFavoriteMovies();
  }

  initFavoriteMovies() {
    this.favoriteMovieProvider.getFavoriteMovies().subscribe(favorites => this.favoriteMovies = favorites);
  }

  findMovie() {
    this.navCtrl.push(MovieListPage);
  }

  goToDetail(movie: IMovie) {
    this.navCtrl.push(MovieDetailPage, movie);
  }
}
