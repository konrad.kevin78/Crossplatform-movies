import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { IMovie } from "../../interface/IMovie";
import { Observable } from "rxjs/Observable";

const MOVIES_KEY = "movies";

@Injectable()
export class FavoriteMovieProvider {

  private movies: IMovie[] = [];

  constructor(private storage: Storage) {
    this.initFavorites();
  }

  private initFavorites() {
    this.storage.get(MOVIES_KEY).then(movies => {if (movies) movies.forEach(movie => this.movies.push(movie))});
  }

  private saveFavorites() : Promise<IMovie[]> {
    return this.storage.set(MOVIES_KEY, this.movies);
  }

  addFavoriteMovie(movie: IMovie) : Promise<IMovie[]> {
    this.movies.push(movie);
    return this.saveFavorites();
  }

  removeFavoriteMovie(movie: IMovie) : Promise<IMovie[]> {
    this.movies.splice(this.movies.indexOf(movie), 1);
    return this.saveFavorites();
  }

  isFavoriteMovie(movie: IMovie) : boolean {
    return this.movies.indexOf(movie) >= 0;
  }

  toggleFavoriteMovie(movie: IMovie) : Promise<IMovie[]> {
    return this.isFavoriteMovie(movie) ? this.removeFavoriteMovie(movie) : this.addFavoriteMovie(movie);
  }

  getFavoriteMovies(): Observable<IMovie[]> {
    return Observable.create(observer => observer.next(this.movies));
  }
}
